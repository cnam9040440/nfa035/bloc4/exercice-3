package fr.cnam.foad.nfa035.badges.gui.controller;

import fr.cnam.foad.nfa035.badges.gui.view.AddBadgeDialog;
import fr.cnam.foad.nfa035.badges.gui.view.DisplayedBadgeHolder;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * Commentez-moi
 */
@Component("addBadgeController")
@Qualifier("jsonBadge")
@Order(1)
public class AddBadgeDialogController {

    @Autowired
    private DirectAccessBadgeWalletDAO dao;

    @Autowired
    private DisplayedBadgeHolder displayedBadgeHolder;


    /**
     * Valide si les champs sélectionnées suffise à la constitution d'un badge
     * @return boolean
     */
    public boolean validateForm(AddBadgeDialog view){
        File image = view.getFileChooser().getSelectedFile();
        String codeSerieStr = view.getCodeSerie().getText();
        Date fin = view.getDateFin().getDate();
        Date debut = view.getDateDebut().getDate();
        if (codeSerieStr != null && codeSerieStr.trim().length() > 4 && image != null && image.exists()
                && ((fin != null && debut == null)||(fin != null && fin.after(debut)))){
            displayedBadgeHolder.setDisplayedBadge(new DigitalBadge(codeSerieStr,debut,fin,null,image));
            return true;
        }
        else{
            return false;
        }
    }

    public void delegateOnOk(DigitalBadge badge) throws IOException {
        dao.addBadge(badge);
    }
}
