package fr.cnam.foad.nfa035.badges.gui.model;

import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component("walletDAOFactory")
@Order(value = 1)
public class WalletDAOFactory {
    @Qualifier("jsonBadge")
    @Autowired
    DirectAccessBadgeWalletDAO jsonBadgeDao;

    @Qualifier("directAccess")
    @Autowired
    DirectAccessBadgeWalletDAO directAccessDao;

    @Bean
    @Qualifier("guiSelected")
    @Scope(BeanDefinition.SCOPE_PROTOTYPE)
    public DirectAccessBadgeWalletDAO createInstance(@Value("#{ systemProperties['db.format'] ?: 'jsonBadge'}") String dbFormat) {
        switch (WalletDBFormats.valueOf(dbFormat)){
            case jsonBadge:
                return jsonBadgeDao;
            case directAccess:
                return directAccessDao;
            case simple:
                throw new RuntimeException("Format non supporté");
            default:
                return jsonBadgeDao;
        }
    }

}
