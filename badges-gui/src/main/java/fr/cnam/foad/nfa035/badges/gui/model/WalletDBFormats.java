package fr.cnam.foad.nfa035.badges.gui.model;

public enum WalletDBFormats {
    jsonBadge, directAccess, simple
}
